FROM registry.gitlab.com/wolfpack1/recommender-service/base-image:latest
#################################

COPY src /app

# for run app
CMD ["uwsgi", "--ini", "./uwsgi.ini"]

# dummy start
# CMD tail -f /dev/null


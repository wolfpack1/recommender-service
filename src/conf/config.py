# Configuration file
import sys
import os

my_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, my_path + '/../')

from logging_service import logger as log


# list of valid API versions
valid_versions = ["v1"]

# max number of recommendations per user
max_recommendations = 5


# Secret credentials for deployment
def return_secret_creds(flavor):
    """Return deployment credentials available as environment variables in container."""
    try:
        if flavor == "db":
            password = os.environ['db_password']
            username = os.environ['db_username']
        return (username, password)
    except KeyError:
        #  the credentials above available and needed only in Kubernetes, not in CI/CD pipeline
        return ('', '')
    except Exception as e:
        log.e('Secret credentials retrieval failed with: %s' % e)
        raise Exception('Secret credentials retrieval failed with: %s' % e)


# DB configuration
postgres_connection = {
    "deploy": {
        "hostname": "rztvnode452.cz.tmo",
        "database": "d_ocn",
        "port": "6432",
        "user": return_secret_creds("db")[0],
        "password": return_secret_creds("db")[1]
    },
    "test": {
        "hostname": "postgres",
        "database": "paranoid_mole",
        "user": "runner",
        "password": ""
    },  # CI/CD testing
    "local_test": {
        "hostname": "localhost",
        "database": "postgres",
        "user": "postgres",
        "password": "postgres",
        "port": "5432"
    }  # local testing
}

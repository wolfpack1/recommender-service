import os
import sys

my_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, my_path + '/../')

from conf import config as cfg
from logging_service import logger as log
from db_communicator import db_handler as dbh


class Recommendation:
    def __init__(self, user_id, num_recommendations, connection):
        """
        Creation of payload of recommendations retrieved from DB.
        :param user_id: identifier of a user for which recommendations are being made
        :param num_recommendations: number of recommendations to be returned; currently
            DB holds 5 recommendations for a given user
        :param connection: psycopg2 connection object
        :return: dict with recommendations {"user": user_id, "recommendations": {1: recommendation1, ...}}
        """
        self.user_id = user_id
        self.num_recommendations = num_recommendations
        self.connection = connection
        self.db_response_recommendations = []
        self.payload = dict()
        try:
            assert self.num_recommendations <= cfg.max_recommendations
        except AssertionError:
            log.e("Maximum number of recommendations is %s. Requested number %s is higher"
                  % (cfg.max_recommendations, self.num_recommendations))
            raise Exception("Maximum number of recommendations is %s. Requested number %s is higher"
                            % (cfg.max_recommendations, self.num_recommendations))

    def retrieve_from_db(self, mode):
        """
        Retrieve recommendations from DB for a given user.
        :param mode: local_test / test / deploy
        :return: list of tuples, where tuples are lines from the table
        """
        if mode == "deploy":
            table = "wolflings_recommendations"
        else:
            table = "wolflings_recommendations_test"
        query = "SELECT * FROM %s WHERE customerid = %s AND " \
                "priority <= %s" % (table, self.user_id, self.num_recommendations)

        self.db_response_recommendations = dbh.run_query(self.connection, query)[0]
        return self.db_response_recommendations

    def create_payload(self):
        """Create payload to be returned as response."""
        self.payload["user"] = self.user_id
        self.payload["recommendations"] = {}
        i = 1
        print(self.db_response_recommendations)
        for recom in self.db_response_recommendations:
            self.payload["recommendations"][i] = recom[1]
            i += 1
        return self.payload


# if __name__ == "__main__":
#     import db_communicator.db_connector as dbc
#     connection = dbc.get_connection("local_test")
#     recommendations = Recommendation(12431, 5, connection)
#     recommendations.retrieve_from_db()
#     print(recommendations.create_payload())

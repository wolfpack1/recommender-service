import os
import sys
import pytest

my_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, my_path + '/../')

from validator.validations import Validator


@pytest.fixture
def validator():
    validator = Validator()
    return validator


class TestValidator:
    def test_validate_route(self, validator):
        assert isinstance(validator, Validator)
        assert validator.validate_route("v1")
        assert not validator.validate_route("v2")
        assert not validator.validate_route("covid19")
        assert not validator.validate_route(1)
        assert not validator.validate_route(True)

    def test_payload(self, validator):
        assert isinstance(validator, Validator)

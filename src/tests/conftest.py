import os
import sys
import pytest

my_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, my_path + '/../')

import db_communicator.db_connector as dbc
import db_communicator.db_handler as dbh


def pytest_addoption(parser):
    parser.addoption("--mode", action="store", default="test")


@pytest.fixture(scope='session')
def mode(request):
    mode_value = request.config.option.mode
    if mode_value is None:
        mode_value = "test"
    return mode_value


@pytest.fixture(scope="session")
def connection(mode):
    connection = dbc.get_connection(mode)
    return connection


@pytest.fixture(scope="session", autouse=True)
def recreate_tables(connection):
    query = """
            DROP TABLE IF EXISTS wolflings_recommendations_test;
            CREATE TABLE wolflings_recommendations_test (
            stockcode varchar(20),
            description varchar(255),
            customerid integer,
            priority integer
            );
            INSERT INTO wolflings_recommendations_test (
            stockcode, 
            description,
            customerid,
            priority)
            VALUES
                ('35004B','SET OF 3 BLACK FLYING DUCKS',12431,1),
                ('22024','RAINY LADIES BIRTHDAY CARD',12431,2),
                ('22616','PACK OF 12 LONDON TISSUES',12431,3),
                ('22712','CARD DOLLY GIRL',12431,4),
                ('22983','CARD BILLBOARD FONT',12431,5),
                ('21832','CHOCOLATE CALCULATOR',12583,1),
                ('22381','TOY TIDY PINK POLKADOT',12583,2),
                ('22379','RECYCLING BAG RETROSPOT',12583,3),
                ('21912','VINTAGE SNAKES & LADDERS',12583,4),
                ('22783','SET 3 WICKER OVAL BASKETS W LIDS',12583,5)
        """
    dbh.run_query(connection, query)

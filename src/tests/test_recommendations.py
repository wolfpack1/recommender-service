import os
import sys
import pytest

my_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, my_path + '/../')

from recommendations.retrieve_recommendations import Recommendation


class TestRetrieveRecommendations:
    def test_retrieve_from_db(self, connection, mode):
        recommendations = Recommendation(12431, 5, connection).retrieve_from_db(mode)

        assert recommendations == [('35004B', 'SET OF 3 BLACK FLYING DUCKS', 12431, 1),
                                   ('22024', 'RAINY LADIES BIRTHDAY CARD', 12431, 2),
                                   ('22616', 'PACK OF 12 LONDON TISSUES', 12431, 3),
                                   ('22712', 'CARD DOLLY GIRL', 12431, 4),
                                   ('22983', 'CARD BILLBOARD FONT', 12431, 5)]

        recommendations = Recommendation(12431, 4, connection).retrieve_from_db(mode)

        assert recommendations == [('35004B', 'SET OF 3 BLACK FLYING DUCKS', 12431, 1),
                                   ('22024', 'RAINY LADIES BIRTHDAY CARD', 12431, 2),
                                   ('22616', 'PACK OF 12 LONDON TISSUES', 12431, 3),
                                   ('22712', 'CARD DOLLY GIRL', 12431, 4)]

        with pytest.raises(Exception):
            Recommendation(12431, 6, connection).retrieve_from_db(mode)

    def test_create_payload(self, connection):
        recommendation = Recommendation(12431, 5, connection)
        recommendation.db_response_recommendations = [('35004B', 'SET OF 3 BLACK FLYING DUCKS', 12431, 1),
                                                      ('22024', 'RAINY LADIES BIRTHDAY CARD', 12431, 2),
                                                      ('22712', 'CARD DOLLY GIRL', 12431, 4),
                                                      ('22983', 'CARD BILLBOARD FONT', 12431, 5)]

        assert recommendation.create_payload() == {"user": 12431, "recommendations": {1: 'SET OF 3 BLACK FLYING DUCKS',
                                                                                      2: 'RAINY LADIES BIRTHDAY CARD',
                                                                                      3: 'CARD DOLLY GIRL',
                                                                                      4: 'CARD BILLBOARD FONT'}
                                                   }

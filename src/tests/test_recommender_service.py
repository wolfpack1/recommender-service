import os
import sys
import pytest

my_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, my_path + '/../')

import recommender_service


@pytest.fixture
def client(mode):
    client = recommender_service.create_app(mode).test_client()
    return client


class TestApi:
    @pytest.mark.parametrize('endpoint, response_code', [
        # OK
        ("/recommender/v1/12431/",
         200
         ),
        # invalid version
        ("/recommender/v2/12431/",
         400
         ),
        # endpoint does not exist
        ("/recommendera/v1/12431/",
         404
         ),
        # OK, but user does not exist -> no recommendations
        ("/recommender/v1/9999/",
         200
         )

    ])
    def test_get(self, client, endpoint, response_code):
        response = client.get(endpoint)
        assert response.status_code == response_code

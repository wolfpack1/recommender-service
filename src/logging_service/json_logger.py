# Logging package

import sys
import os
import logging
from pythonjsonlogger import jsonlogger
import datetime
from pytz import timezone

my_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, my_path + '/../')

from logging_service.file_logger import Logger


localtz = timezone('Europe/Bratislava')

supported_keys = [
    'filename',
    'funcName',
    'levelname',
    'message'
]


class JsonLogger(Logger):
    def __init__(self):
        Logger.__init__(self)
        log_format = lambda x: ['%({0:s})'.format(i) for i in x]
        custom_format = ' '.join(log_format(supported_keys))
        logger = logging.getLogger()

        formatter = jsonlogger.JsonFormatter(custom_format)

        # log handler for logging to stdout
        log_handler_std = logging.StreamHandler()
        log_handler_std.setFormatter(formatter)
        logger.addFilter(SystemLogFilter())
        logger.addHandler(log_handler_std)


class SystemLogFilter(logging.Filter):
    @staticmethod
    def filter(record):
        record.datetime = localtz.localize(datetime.datetime.now()).strftime("%Y-%m-%dT%H:%M:%S.%f%z")
        return True

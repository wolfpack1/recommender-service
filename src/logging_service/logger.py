# Logging package

import os
import sys

my_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, my_path + '/../')

from conf import log_config as cfg
from logging_service.file_logger import FileLogger
from logging_service.json_logger import JsonLogger


if cfg.log_env == 'local':
    log = FileLogger()
else:
    log = JsonLogger()


def i(msg, **kwargs):
    """Shorthand for logging of info msg."""
    global log
    app_log = log.logger
    app_log.info(msg, **kwargs)


def w(msg, **kwargs):
    """Shorthand for logging of warning msg."""
    global log
    app_log = log.logger
    app_log.warning(msg, **kwargs)


def e(msg, **kwargs):
    """Shorthand for logging of error msg."""
    global log
    app_log = log.logger
    app_log.error(msg, **kwargs)

# Logging package

import os
import sys
import logging
from logging.handlers import RotatingFileHandler

my_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, my_path + '/../')

from conf import log_config as cfg


class Logger:
    def __init__(self):
        self.logger = logging.getLogger()
        self.logger.setLevel(cfg.log_level)


class FileLogger(Logger):
    def __init__(self):
        Logger.__init__(self)
        formatter = logging.Formatter('%(asctime)s %(levelname)s: %(msg)s', '%Y-%m-%d %H:%M:%S')
        handler = RotatingFileHandler(cfg.log_fileName, maxBytes=cfg.log_size, backupCount=10)
        handler.setFormatter(formatter)
        self.logger.addHandler(handler)

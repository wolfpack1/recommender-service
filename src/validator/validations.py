# Validations

import os
import sys

my_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, my_path + '/../')

from conf import config as cfg


class Validator:
    def __init__(self):
        pass

    @staticmethod
    def validate_route(version):
        """
        Validate that user provided version of api is valid.
        :param version: version of api, e.g. "v1"
        :return: True if provided version is valid, False otherwise
        """
        if version not in cfg.valid_versions:
            return False
        return True

    def validate_payload(self):
        pass

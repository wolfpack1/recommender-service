# Recommendations service API
# Author: Radoslav Pauco
# Email: radoslav.pauco@adastragrp.com

from argparse import ArgumentParser
from flask import Flask, jsonify, Blueprint
from flask_restx import Resource, Api

from validator.validations import Validator
import db_communicator.db_connector as dbc
from recommendations.retrieve_recommendations import Recommendation


def create_app(mode):
    """
    Application factory.
    :param mode: "local_test" or "test" or "deploy"
    :return: Flask app object
    """
    app = Flask(__name__)
    app.config['mode'] = mode
    print('Passed mode: ', app.config['mode'])

    # flask restx Api instance
    blueprint = Blueprint('recommender', __name__, url_prefix='/recommender')
    api = Api(blueprint,
              version='1.0',
              title='Recommendations service API',
              description="Ultimately awesome recommender for wolflings.",
              doc='/doc/',
              contact="radoslav.pauco@adastragrp.com")

    app.register_blueprint(blueprint)

    @api.route("/<string:api_version>/<string:user_id>/")
    class Main(Resource):
        """
        Primary endpoint for retrieving recommendations.
        """
        @api.doc(responses={
            200: 'OK',
            400: 'Invalid endpoint.'})
        def get(self, api_version, user_id):
            """
            Get request handler.
            :param api_version:
            :param user_id:
            :return: recommendations json
            """
            # validate api version
            validator = Validator()
            if not validator.validate_route(api_version):
                response = jsonify({"success": "false",
                                    "error": "Invalid api version in the endpoint."})
                response.status_code = 400
                return response

            # validate user_id format
            # TODO: implement validation of user_id, return special error code for not existing user/invalid format

            # probe
            if user_id == "probe_user":
                response = jsonify({"success": "true"})
                response.status_code = 200
                return response

            # TODO: demonstrating worsening of code quality

            # retrieve recommendation from DB for user_id
            connection = dbc.get_connection(mode)
            recommendations = Recommendation(user_id, 5, connection)
            recommendations.retrieve_from_db(mode)

            # create response payload
            payload = recommendations.create_payload()
            response = jsonify({"success": "true",
                                "body": payload})

            # default dummy response
            # response = jsonify({"success": "true",
            #                     "body":
            #                         {"user_id": user_id.lower(),
            #                          "recommendations": {"1st": "rúško",
            #                                              "2nd": "remdesivir",
            #                                              "3rd": "rukavice"}}
            #                     })

            response.status_code = 200
            return response

    return app


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('-a')
    args = parser.parse_args()
    app = create_app(args.a)
    app.run(debug=True)

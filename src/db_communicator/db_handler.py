# DB Communicator package
# Handling DB queries

import os
import sys
import time
import psycopg2

my_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, my_path + '/../')

from logging_service import logger as log


def run_query(connection, query):
    """
    Run query against DB.
    :param connection: DB connection - psycopg2 connection object
    :param query: query to be run
    :return: query result, row count
    """
    log.i('Running query: ' + query)
    t = time.time()
    row_cnt = 0
    try:
        cur = connection.cursor()
        cur.execute(query)
        connection.commit()
        row_cnt = cur.rowcount
    except psycopg2.IntegrityError:
        connection.rollback()
        cur.close()
        log.i('PK violated.')
        raise Exception("PK violated.")
    except Exception as e:
        log.e('Query failed: %s' % query)
        log.e('Exception detail: %s' % e)
        connection.rollback()
        cur.close()
        raise Exception('Query execution failed with: %s' % e)
    try:
        result_set = cur.fetchall()
        log.i('Query result: ' + str(result_set))
    except Exception as e:
        log.i('Query returned no results because fetchall() method raised exception: %s' % e)
        result_set = None
    finally:
        cur.close()
    delta = time.time() - t
    log.i('Query run time seconds: ' + str(delta))
    return result_set, row_cnt

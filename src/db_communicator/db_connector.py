# DB Communicator package
# DB Connection

import os
import sys
import psycopg2

my_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, my_path + '/../')

from logging_service import logger as log
from conf import config as cfg

# global connection object
conn = None


def get_connection(flavor):
    """
    Get psycopg2 connection object for given postgres connection parameters.
    :param flavor: either test or test_local or deploy connection parameter set,
        retrieved from config
    :return: psycopg2 connection object
    """
    global conn

    if conn and conn.closed == 0:
        # if connection is defined and still alive
        return conn

    conn_cfg = cfg.postgres_connection.get(flavor)
    user = conn_cfg.get('user')
    db = conn_cfg.get('database')
    host = conn_cfg.get('hostname')
    pw = conn_cfg.get('password', '')
    try:
        port = conn_cfg.get('port', '')
        log.i('Creating postgres connection: ' + user + '@' + host + ':' + port + '/' + db)
        conn = psycopg2.connect(database=db, user=user, password=pw, host=host, port=port)
    except KeyError:
        try:
            conn = psycopg2.connect(database=db, user=user, password=pw, host=host)
        except Exception as e:
            log.e('Connection to postgres DB failed!')
            log.e('Exception detail: %s' % str(e))
            raise Exception(e)
    except Exception as e:
        log.e('Connection to postgres DB failed!')
        log.e('Exception detail: %s' % str(e))
        raise Exception(e)

    return conn
